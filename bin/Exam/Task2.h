#include<iostream>
#include<memory>
#include<vector>

struct List {
    unsigned value;

    std::unique_ptr<List> n;

    List(unsigned v, std::unique_ptr<List> next) : value(v), n(std::move(next)) {}

    List(unsigned v) : value(v) {}

    ~List() {
        List* l = n.release();
        delete l;
    }

};


/*
A unique pointer has just one reference on it, once it is remove the unique pointer automatically frees its references,
the problem here is that c++ want to free all elements of the list, but they are already freed after the first
pointer was released.
*/

int main() {
    std::unique_ptr<List>  head(new List(1, std::unique_ptr<List>(new List(0))));

    for (int i = 2; i < 100000; i++) {
        std::unique_ptr<List> head2(new List(i,std::move(head)));
        head = std::move(head2);
    }

    std::cout << "Printed !" << std::endl;

    return 0;
}


