#include <iostream>
#include <functional>
#include <set>
#include <string>
#include <algorithm>
#include <cstring>

using std::set;
using std::string;

int main() {
    set<string> text;
    text.insert("En");
    text.insert("un");
    text.insert("lugar");
    text.insert("de");
    text.insert("la");
    text.insert("mancha");
    text.insert("de");
    text.insert("cuyo");
    text.insert("nombre");
    text.insert("no");
    text.insert("quiero");
    text.insert("acordarme");
    text.insert("vivia");
    text.insert("un");
    text.insert("hidalgo");
    text.insert("llamado");
    text.insert("don");
    text.insert("quijote");

    set<string> keys;
    keys.insert("vivia");
    keys.insert("acordarme");
    keys.insert("quijote");
    keys.insert("sancho");

    std::find_if(text.begin(), text.end(), [keys, text] (const string t){
        std::cout << "Check text: " << t << std::endl;
        return (std::find_if(keys.begin(), keys.end(), [t] (const string k){
            std::cout << "\tCheck key: " << k << "[" << (t == k) << "]" << std::endl;
            return t == k;
        })) != keys.end();
    });

    // Check all words within keys are included in text
    // Restrictions: you got to do it using std::find_if
    // and lambda functions. You can use an O(n^2) solution

};
