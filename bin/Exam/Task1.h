//
// Created by cle1000 on 20.01.17.
//

#ifndef EXAM_TASK1_H
#define EXAM_TASK1_H
#include <cstddef>


template <size_t N, size_t M>
struct mcd {
    static const size_t value =  mcd<M, N % M>::value ;
};

template <size_t N>
struct mcd<N, 0>{
    static const size_t value = N;
};

#endif //EXAM_TASK1_H
