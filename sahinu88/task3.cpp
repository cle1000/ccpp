#include <iostream>
#include <functional>
#include <set>
#include <string>
#include <algorithm>
#include <vector>

using std::set;
using std::string;

int main() {
	set<string> text;
	text.insert("En");
	text.insert("un");
	text.insert("lugar");
	text.insert("de");
	text.insert("la");
	text.insert("mancha");
	text.insert("de");
	text.insert("cuyo");
	text.insert("nombre");
	text.insert("no");
	text.insert("quiero");
	text.insert("acordarme");	
	text.insert("vivia");
	text.insert("un");
	text.insert("hidalgo");
	text.insert("llamado");
	text.insert("don");
	text.insert("quijote");

	set<string> keys;
	keys.insert("vivia");
	keys.insert("acordarme");
	keys.insert("quijote");
	//keys.insert("sancho");

	// Your code here.
	// Check all words within keys are included in text
	// Restrictions: you got to do it using std::find_if
        // and lambda functions. You can use an O(n^2) solution
        
    std::vector<bool> found;
    
    std::set<string>::iterator it = std::find_if(
        text.begin(),
        text.end(),
        [ keys, &found ]( string s1 ){
            for ( auto &s2 : keys ){
                if ( s1.compare( s2 ) == 0 )
                    found.push_back( true );
            }
            return false;
        }
    );
    
    std::cout << ( ( keys.size() == found.size() ) ? "all included" : "not all included" ) << std::endl;	
						
};