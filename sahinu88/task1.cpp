#include <iostream>

template<bool condition, typename type1, typename type2>
struct typeif {
	typedef type1 type;
};

template<typename type1, typename type2>
struct typeif<false, type1, type2> {
	typedef type2 type;
};

// basic gcd function
unsigned sahinu88_gcd( unsigned N, unsigned M ){
    return ( M == 0 ) ? N : sahinu88_gcd( M, N % M );
}

// template gcd function
template<typename T> T sahinu88_template_gcd( T N, T M){
    return ( M == 0 ) ? N : sahinu88_template_gcd( M, N % M );
}

// struct for gcd
template <size_t N, size_t M>
struct sahinu88_struct_gcd {
    static const size_t value = sahinu88_struct_gcd<M, N % M>::value;
};

template <size_t N>
struct sahinu88_struct_gcd<N, 0>{
    static const size_t value = N;
};

int main() {
    // I changed the functions name, hope that's fine
    
    // simple function for gcd, just for testing
    unsigned m1 = sahinu88_gcd( 26u, 2032u );
    
    // template gcd
    unsigned m2 = sahinu88_template_gcd( 26u, 2032u );
    
	// I expect to be able to call your template as follows
	unsigned m3 = sahinu88_struct_gcd<26u,2032u>::value;
    
	std::cout << m1 << std::endl;
	std::cout << m2 << std::endl;
	std::cout << m3 << std::endl;
}
